.. _developer_guide :

.. toctree::
   :maxdepth: 4
   :caption: Contents:

Developper guide
================

Workflow
--------

Right before getting to work on your model, let's just see quickly the workflow you could implement so that everything works smoothly.  

* Modifying Adjustin package only locally

When adding your model it is possible that you will end up wanting to add/modify files in the Adjustin package.
Because you can't directly change this package, a workaround is to ``pkg> dev --local https://gitlab-research.centralesupelec.fr/2009viaudg/adjustin.git``  
This replace the command ``pkg> add https://gitlab-research.centralesupelec.fr/2009viaudg/adjustin.git`` as seen in the installation guide.

Tip : If you want to make your workflow faster, you can download the package Revise by doing ``pkg> add Revise``.
You then only need to add ``using Revise`` before your ``using Adjustin``. This will allow you to avoid closing Atom and compiling again the whole Adjustin package everytime you make a change to it.

* Modifying Adjustin package on gitlab

The steps are the same as in the previous section, however if you want to afterwards push your change to gitlab, there's a few more steps that need to be done.  

**Before modifying any files** of the package, 

1. Create a new branch for this project 
2. Open a command line (git bash) and go to the root directory of the local package of Adjustin that you were using up until now. It contains the folder .git (.julia/environment/[julia_version]/dev/Adjustin)  
3. Indicate the branch you want to work on `` git checkout your_branch_name``. If branch name is not recognised try ``git fetch`` and then checkout again

You are now free to make any changes you like.  

And finally, when you want to push back the changes on git, simply do : 

1. ``git add .``
2. ``git commit -m "message"``
3. ``git push origin your_branch_name``

Add probability fucntion
------------------------

Add noises
----------

Add algorithms
--------------

Add models
----------

Add simulations
---------------



