.. _user_guide :

.. toctree::
   :maxdepth: 4
   :caption: Contents:


User guide
==========

testing working example
-----------------------
Let's try out one of the working examples. 

In an editor, open the file adjustinapplications>applications>specific>gls-lnas
Select the whole code and press ctrl + enter 
If everything is working this should execute the selected code otherwise an error will be thrown.

understanding Adjustin
----------------------

Adjustin structure
^^^^^^^^^^^^^^^^^^

::

   src
      algorithms                              #store all the algorithms
         |automata
         |estimation
               helpers
               algo_*.jl
         |sensitivity_analysis
      core
         |filters.jl                         #handles use of particles and indices
         |helpers.jl                         
         |input_output.jl                    #functions to import files from database, write files in results folder...
         |math.jl
         |noise.jl
         |observations.jl
         |probability_density_functions.jl
         |pygmalion.jl
         |sampling.jl
         |simulation.jl
         |system_observations.jl
         |vectorize.jl
      etc
         |dictionaries.jl
         |multiindex.jl
         |plot.jl
      models
         |your_model.jl
      Adjustin.jl


Adjustin application structure
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

::

   application
      |generic                    #generic examples of applications using various algorithms
      |specific                   #specific examples of applications using various algorithms
      |your_app_here              #where you will write your own application
   database                        #store model variables that will be loaded (cf. applications code) and then use in your code
      |configuration              
         estimation
         sensitivity_analysis
      |control
      |observations
      |parameters
      |state
   results                         #store the results of every execution, files inside can be deleted once out of use
   README




Mathematical framework
^^^^^^^^^^^^^^^^^^^^^^

.. math::

   x_{n+1} &= f(x_{n}, u_{n}, \theta_{n}, mn_{n}) \\
   y_{n} &= g(x_{n}, \theta_{n}, on_{n})

* :math:`x_{n}` represents the state variables
* :math:`y_{n}` represents the observation on the system
* :math:`u_{n}` represents the external variables (the environment) influencing the system
* :math:`\theta` represents the parameters of our model
* :math:`mn_{n}` represents the modelling/process noise - accounts for either possible model limitations or imperfections
* :math:`on_{n}` represents the observation noise - accounts for the measurement errors

 
The objective is to understand statistical systems thanks to the estimation of model parameters using experimental data.
We want to create a model which modelised the best way possible what we want to study in order to analyse it.
In other terms the objective is to find f, :math:`\theta`, mn, on, so that the experimental observations fit the best way possible y for each time step.
Note that x can either be hidden state variables or observed data. 
y is x filtered in order to keep only the state variable that were actually measured experimentally (which exclude the hidden state variables) 
For example, imagine you run a simulation, it will compute every variable of the vector x at each time step. 
But what you observed in real life will only be on specific variable at specific time. 
The function g will thus filter x to keep only the observed variable of vector x.



Adjustin structures & functions
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ 


1. structures

* x, u, p (the mathematical :math:`\theta`) are mutable structs with fields of your choice
* mn, on are mutable structs with field of type Noise. The type noise is itselft a mutable struct with the fields : 

.. code-block:: julia

   mutable struct Noise
      ll_det::LabelList  #the name of the deterministic variable
      ll_sto::LabelList  #the name of the stochastic variable
      kind::Label        #distribution type
      vl::Tuple          #parameters of this distribution like (mean, standard deviation) or (alpha, beta)...
   end

* so_* stands for system of observation, they are mutable struct of the form :

.. code-block:: julia

   mutable struct SystemObservation
      title::Label
      oml::LabelList                      #list of names of the observed states : ["om1","om2","om3"]
      otll::Array{ObservationTupleList}   #list of values (vl) at each time step for each observed state : 
                                          #[[(t1,[x_t1_om1]),(t2,[x_t2_om1]),...,(tn,[x_tn_om1])],
                                          # [(t1,[x_t1_om2]),(t2,[x_t2_om2]),...,(tn,[x_tn_om2])],
                                          # [(t1,[x_t1_om3]),(t2,[x_t2_om3]),...,(tn,[x_tn_om3])]]
      set::Function                       #function to set a new tuple in otll : so.set(o.om, t, vl)
      print::Function
   end

* A list of Observer represents in Adjustin the mathematical function g in the sense that it is a struct that contains the necessary informations to recover the observed states. One Observer is a mutable struct of the form : 

.. code-block:: julia

   struct Observer
      om::Label            #name of one field of the state x or name of an observation_noise
      f::Function          #function to retrieve the value of "om"
      tml::Timeline        #every times on which om is going to be computed
      nbr_args::Int        #number of arguments that f takes as input
   end

* The observer model is used to create the Observer. It's a mutable struct of the form :

.. code-block:: julia

   struct ObserverModel
      om::Label
      tml::Timeline
   end

2. functions

* create_observation_function(ol) creates a list of Observer (i.e what stand for the mathematical g function) with ol, a list of ObserverModel, as input 

* f, the transition function, is as decribed mathematically

* simulate(f, g, x0, u, p; mn=nothing, on=nothing) is a function that will return a SystemObservation with the values of only the observed state variables on the timeline defined in g. To do so it will launch the function f at every timestep (timestep being 1), computing every field of the state, but will then keep in memory in the SystemObservation only the values on the desired timeline for the desired field of state x.


Adding your model : Detailed template
-------------------------------------

We can now finally start adding our model.  
Note that in the whole section we will use the notation *TC_* to indicate code that needs to be change.  
For example "TC_model_name" can become "greenlab-arabidopsis"


* main file .jl template in adjustin application

.. code-block:: julia

   ENV["ADJUSTIN_PATH_DATABASE"] = string(@__DIR__, "/database/")
   ENV["ADJUSTIN_PATH_RESULTS"] = string(@__DIR__, "/results/")

   using Adjustin


.. code-block:: julia

   str_m = "TC_model_name"

   str_c = "TC_controle_file_name"
   str_x = "TC_state_file_name"
   str_p = "TC_parameters_file_name"
   str_cfg = "TC_configuration_file_name"

   str_d = "TC_results_part_of_file_name"  
   str_dir =  create_results_direTCory(; title = str_d)  

   load_local_model(str_m)  #use load_model(str_m) instead if you are using a preexistant model defined in Adjustin package
   load_state(str_m, str_x)
   load_control(str_m, str_c)
   load_parameters(str_m, str_p)
   load_configuration("algo*",str_m, str_cfg) # for example load_configuration("gls",str_m, str_cfg)


If you are using the functions load_*, which unable to have a cleaner code and not everything in a single file
(you could also not use the functions load_* and declare directly your parameters, states, controls and configurations directly in your main file) 
then you must be careful of the names put as input of those functions.
Indeed the functions load* will look for the input name in a specific location. You must make sure these are correct (see below).
Note that you will have to create those files in the appropriate location. 
Note also that in the example above we loaded a local model which is in the Adjustinapplication repository, in the database (see below).
However if you are using a preexisting model in Adjustin (Adjustin comes with a set of precoded model), 
the model file will be in the Adjustin package (not adjustinapplications) located at src/models/TC_model_name.jl.

::

   database                       
      |configuration              
         |estimation
            |algo*
               |TC_model_name
                  TC_configuration_file_name
         |sensitivity_analysis
            |algo*
               |TC_model_name
                  TC_configuration_file_name
      |control
         |TC_model_name
            TC_controle_file_name.jl
      |observations
         |TC_model_name
      |parameters
         |TC_model_name
            TC_parameters_file_name.jl 
      |state
         |TC_model_name
            TC_state_file_name.jl 
      |models
         |TC_model_name    #if you add your own model and not use a preexisting one in adjustin


.. code-block:: julia

   on = ObservationNoise(Noise(
      ["TC_field_name_observed"],
      ["obs_TC_field_name_observed"],
      "TC_kind",                   
      (TC_int_param_noise_1, TC_int_param_noise_2),
   ))


The observation noise, on, is optionnal  
TC_field_name_observed must be the same as one of the field in the struct ObservationNoise in Adjustin:src/models/TC_model_name.jl 
which is also the same as one of the field in the struct State in Adjustin:src/models/TC_model_name.jl  

obs in the beginning of obs_TC_field_name_observed must be there.

TC_kind can be : additive-normal/multiplicative-normal/multiplicative-lognormal/additive-uniform/multiplicative-uniform  

How TC_int_param_noise_1 and 2 are used is detailed in Adjustin src/core/noise.jl in function noise!()   



.. code-block:: julia

   mn = ModellingNoise(Noise(
      ["TC_field_name_det"],
      ["TC_field_name_sto"],
      "TC_string_kind",
      (TC_int_param_noise_1, TC_int_param_noise_1)),


The modelling noise mn is optionnal  
TC_field_name_det (det for deterministic) must be the same as one of the field in the struct State in Adjustin:src/models/TC_model_name.jl  
TC_field_name_sto (sto for stochastic) must be the same as one of the field in the struct State in Adjustin:src/models/TC_model_name.jl  
)


.. code-block:: julia

   tmax = TC_int
   tml = 1:tmax
   g = create_observation_function([ObserverModel("all,obs_TC_field_name_observed", tml)])

g will be a list of Observer. '\r\n'
As seen above, an observer is a structure containing the name of the object we want to observe (usually fields in state and/or observation noise), a function to retrieve the value of this object, the timeline on witch we are observing it, and the number of arguments that the function needs.   
`all` (see above) is optional  
it is added (usually more in simulations than estimations) if you want to observed all fields in the state
and not only the stochastic (i.e observed) ones

OR

.. code-block:: julia

   so_exp = load_from_obs("database/observations/TC_model_name/TC_observation_file_name.obs")
   g = create_observation_function(eval(Meta.parse(get_observation_function(so_exp))))

This is another way to compute g, if observation data are imported (for estimation) and you want to make sure 
that you have matching (.i.e same timeline and same observed variables) simulation data and observation data to be able to compare them. 


.. code-block:: julia

   so = simulate(f, g, x0, u, p; mn = mn, on = on)

We can then launch the simulation

OR 

.. code-block:: julia

   cfg = ConfigurationTC_algo_estimation(...)
   so = TC_algo_estimation(f, g, x0, u, p, so_exp, cfg; mn = mn, on = on,)

the estimation algorithm   
see cfg parameters in Adjustin : src/algorithms/estimation/TC_algo_estimation.jl

OR 

.. code-block:: julia

   cfg = ConfigurationTC_algo_sensitivity_analysis(...)
   so = TC_algo_sensitivity_analysis(f, g, x0, u, p, cfg; mn = mn, on = on)

the sensitivity_analysis  
see cfg parameters in Adjustin : src/algorithms/sensitivity_analysis/TC_algo_sensitivity_analysis.jl


.. code-block:: julia

   save_to_obs(TC_what_you_want_to_save, str_dir * "TC_what_you_want_to_save.obs")

We can then explicitly save the results but by activating flag_plot or flag_write in the algorithme configuration, there is automatically saved results


* model.jl template - located in Adjustin  : src/models/model.jl

First all the struct must be defined

.. code-block:: julia

   mutable struct State
      TC_fields1 :: TC_type_fields1
      TC_field_name_observed :: TC_type_field_name_observed
      TC_field_name_det :: TC_type_field_name_sto #cf code *.jl above (lign starting with : "mn = ModellingNoise")
      TC_field_name_sto :: TC_type_field_name_sto #cf code *.jl above (lign starting with : "mn = ModellingNoise")
   end

   mutable struct Control
      TC_fieldc1 :: TC_type_fieldc1
      TC_fieldc2 :: TC_type_fieldc2
   end

   mutable struct Parameters
      TC_fieldp1 :: TC_type_fieldp1
      TC_fieldp2 :: TC_type_fieldp2
   end

   #OPTIONNAL
   mutable struct ModellingNoise
      TC_fieldm1 :: TC_type_fieldm1
      TC_fieldm2 :: TC_type_fieldm2
   end

   #OPTIONNAL
   mutable struct ObservationNoise
      #cf main file .jl above (line starting with : "on = ObservationNoise")
      TC_field_name_observed :: TC_type_field_name_observed #same field name as in one field of state
   end

Then the function returning the state at the next time step must be defined

.. code-block:: julia

   function f(n, xn, u, p)
      return xnplus1
   end


If there is a modelling noise in the model then it must be explicitly put as a parameter of f

.. code-block:: julia

   function f(n, xn, u, p, mn)
      return xnplus1
   end

Finally this next part must be there if we have observation noises on a state variable
As we saw above when defining `g`, `g` is a list of struct defining the object we want to observed  
If we observe the state variable TC_field_name_observed then ofl will be used to pass to the observer 
the observer function (here obs_TC_field_name_observed) that will be used to add a noise to the state variable TC_field_name_observed

.. code-block:: julia

   obs_TC_field_name_observed (x,on) = noise(x, on.TC_field_name_observed)
   ofl = [obs_TC_field_name_observed]

If we observer multiple noise, you just add them to the list ofl, as such :

.. code-block:: julia

   obs_TC_field_name_observed1 (x,on) = noise(x, on.TC_field_name_observed1)
   obs_TC_field_name_observed2 (x,on) = noise(x, on.TC_field_name_observed2)
   ofl = [obs_TC_field_name_observed1, obs_TC_field_name_observed2]

This line will defined the macro observation_function implemented in the file simulation.jl in Adjustin/core.
In other words it's as if the functions create_observation_function() and create_observer_function() defined in the macro
observation_function were implemented in the current file with your model.
This line needs to be here so to be able to used those two functions later on. (cf. main file .jl)
It also needs to be called after ofl is defined (if you have observation noises in your model) because the variable ofl
will be used in the function create_observer_function().

.. code-block:: julia

   @observation_function()


Try out algorithms
------------------


There is two main way to estimate the parameters of the model, frequentist and bayesian estimation.
The main difference between way is the interpretation that they attribute to the nature of parameters.

1. frequential statistic
   
   The parameters have true fixed values and confidence intervals.
   
   Probability of a parameter is interpreted as a frequency of a phenomenon.

2. bayesian statistic
   
   The parameters do not have fixed values but are instead considered to be random variables that follow probability distributions.
   
   Probability of a parameter is interpreted as a degree of belief.
   Better to use when there is prior knowledge that needs to be incorporated to the model or when there is few noisy observations

   There is 2 main family of method : 
      * mcmc (markov chain monte carlo)  -> when you have a big set of data to use in one go
      * smc (sequential monte carlo)  -> when data comes one at a time

3. Algorithm implemented in Adjustin

Estimation algorithms
- abc_pop_mc
- abc_simple
- abc_smc
- abc_subsim  
- amwg
- cpf
- enkf
- gls_vector
- gls
- ip
- kf  
- mcmc_abc
- mcmc_simplified
- parallel_abc
- pmmh  
- sir
- smc_abc
- ukf
- wls  

Sensitivity_analysis algorithms 
- gsobol
- sobol_gls
- sobol  
Known bugs
----------



