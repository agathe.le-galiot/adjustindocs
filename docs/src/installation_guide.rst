.. _installation_guide :

.. toctree::
   :maxdepth: 4
   :caption: Contents:

Installation guide
==================

* Download julia
* Download an editor (Atom for example) and the package associated to this editor to work on julia code (uber-juno if working with Atom -> file/settings/install/uber-juno)

If you chose Atom as your editor, you can now access julia REPL by opening Atom.

Note that you can configure what you see when you open Atom, including the REPL if it's not automaticaly opened (file/settings/Packages/julia-client/settings/UI option/Layout options)

Note also that when you access the julia REPL, you should see by default ``julia>``, press key "]" (i.e "alt gr" + ")" ) to see ``pkg>``

Remember that if you want to check the installed package, you can do so by writting ``pkg> status``. 
You can use it at any time during installation to check if everything was done properly.

Adjustin
--------

IMPORTANT : This git depository must not be cloned on your computer. It is a julia package it thus need to be added like you would do for a julia package (cf. below).

Installation for use only
^^^^^^^^^^^^^^^^^^^^^^^^^

1. master branch

Write and execute the command below :

.. code-block:: julia

   pkg> add https://gitlab-research.centralesupelec.fr/2009viaudg/adjustin.git

2. specific branch

Write and execute the command below :

.. code-block:: julia

   add https://gitlab-research.centralesupelec.fr/2009viaudg/adjustin.git#galiota_branch

Installation for use and developement
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

1. master branch

Write and execute the command below :

.. code-block:: julia

   pkg> dev --local https://gitlab-research.centralesupelec.fr/2009viaudg/adjustin.git

Note that you ``dev`` a package and not ``add`` it so to be able to change it.
If, in the past, you have already added the package Adjustin, you might end up with different Adjustin package on your computer which is not a problem.

You can check the package currently used by going in a julia REPL and executing the command :

.. code-block:: julia

   pkg> status

The first line will indicate from where the packages are recovered (Status C:/Users/MICS/.julia/environments/v1.3/Project.toml).

* When Adjustin is imported with ``dev``:
   The Adjustin package will be in C:/Users/MICS/.julia/environments/v1.3/dev/Adjustin.
   When doing ``pkg>status`` you will see *dev/Adjustin* next to the Adjustin pkg.
* When it is imported with ``add`` :
   It will be in C:/Users/MICS/.julia/packages/Adjustin.

This can of course be slightly different depending on julia's version, computer.
If the packages are not recovered from where you want them to be, then from the julia REPL go to the path where the correct file Project.toml is located by using ``cd("path")`` and then execute the command below :

.. code-block:: julia

   pkg> activate .



2. specific branch

Follow exactly the same steps as for the master branch.
However if you want to use the code of another branch, continue with the following steps.

In git bash terminal go to path to Adjustin but remember that they might be multiple path to Adjustin and that the one you want is the one in *[PATH_TO_JULIA]/.julia/environments/[JULIA_VERSION]/dev/Adjustin*

.. code-block:: julia

   cd path_to_Adjustin

You can then execute the commands :

.. code-block:: julia

   git fetch origin galiota_branch:galiota_branch
   git checkout galiota_branch



Adjustin application
--------------------

1. master branch

In git bash terminal, in the directory where you want to store your applications

.. code-block:: julia

   git clone https://gitlab-research.centralesupelec.fr/2009viaudg/adjustinapplications.git
  

2. specific branch

In git bash terminal, in the directory where you want to store your applications

.. code-block:: julia

   git clone --branch galiota_branch https://gitlab-research.centralesupelec.fr/2009viaudg/adjustinapplications.git
 

