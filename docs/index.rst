.. Adjustin documentation master file, created by
   sphinx-quickstart on Tue May  4 16:09:07 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

####################################
Welcome to Adjustin's documentation!
####################################

.. toctree::
   :maxdepth: 4
   :hidden:

   src/installation_guide.rst
   src/user_guide.rst
   src/developer_guide.rst

The Adjustin package is a julia package for model simulation, parameters estimation and sensitivity_analysis.
The Adjustin applications repository contains example of how to use this package and with which folder organisation.


.. topic:: Installation guide
   
   :ref:`To installation_guide <installation_guide>`

.. topic:: User guide
   
   :ref:`To user guide <user_guide>`

.. topic:: Developper guide
   
   :ref:`To developer guide <developer_guide>`


